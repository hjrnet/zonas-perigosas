package br.com.projeto.zonasperigosas.dao;

import java.io.Serializable;

import javax.persistence.EntityManager;

import br.com.projeto.zonasperigosas.modelo.Ocorrencia;
import br.com.projeto.zonasperigosas.util.JPAUtil;



public class OcorrenciaDao implements Serializable {
	private static final long serialVersionUID = 1L;
	
	public void adiciona(Ocorrencia ocorrencia) {
		EntityManager manager = new JPAUtil().getEntityManager();
		manager.getTransaction().begin();

		//persiste o objeto
		manager.persist(ocorrencia);
		
		manager.getTransaction().commit();
		manager.close();
	}
	
	
}
