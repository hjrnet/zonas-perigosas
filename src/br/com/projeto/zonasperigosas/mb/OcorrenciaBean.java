package br.com.projeto.zonasperigosas.mb;

import java.io.Serializable;

import br.com.projeto.zonasperigosas.dao.OcorrenciaDao;
import br.com.projeto.zonasperigosas.modelo.Ocorrencia;

public class OcorrenciaBean  implements Serializable{
	private static final long serialVersionUID = -1957176543273170800L;

	private OcorrenciaDao dao =new OcorrenciaDao();
	private Ocorrencia ocorrencia =new Ocorrencia();
	
	public String grava(){
	
			dao.adiciona(ocorrencia);
	
	   
		return "gravado";
	}

	public OcorrenciaDao getDao() {
		return dao;
	}

	public void setDao(OcorrenciaDao dao) {
		this.dao = dao;
	}

	public Ocorrencia getOcorrencia() {
		return ocorrencia;
	}

	public void setOcorrencia(Ocorrencia ocorrencia) {
		this.ocorrencia = ocorrencia;
	}
	
	
}
