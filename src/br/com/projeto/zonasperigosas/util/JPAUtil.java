package br.com.projeto.zonasperigosas.util;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;

public class JPAUtil {
	
	private static EntityManagerFactory emf = Persistence.createEntityManagerFactory("hjrnet");

	public EntityManager getEntityManager() {
		return emf.createEntityManager();
	}
	
	
	
}